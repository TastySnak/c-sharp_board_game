﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Square_Class_Library;

namespace Board_Class_Library {

    public static class Board {

        public const int START = 0;
        public const int FINISH = 55;

        const int NUM_OF_SQUARES = 56;

        private static Square[] gameBoard = new Square[NUM_OF_SQUARES];

        public static void SetUpBoard()
        {
            for (int i = 1; i < FINISH; i++)
            {
                if (i % 10 == 0)
                {
                    gameBoard[i] = new Lose_Square(i.ToString(), i);
                }

                else if (i % 5 == 0)
                {
                    gameBoard[i] = new Win_Square(i.ToString(), i);
                }

                else if (i % 6 == 0)
                {
                    gameBoard[i] = new Chance_Square(i.ToString(), i);
                }

                else
                {
                    gameBoard[i] = new Square(i.ToString(), i);
                }
            }

            gameBoard[START] = new Square("Start", START);
            gameBoard[FINISH] = new Square("Finish", FINISH);
        }

        public static Square GetGameBoardSquare(int sqr)
        {
            if (sqr <= FINISH && sqr >= START)
            {
                return gameBoard[sqr];
            }
            else
            {
                throw new ArgumentOutOfRangeException("Parameter is not a valid square number!");
            }
        }

        public static Square StartSquare()
        {
            return gameBoard[0];
        }

        public static Square NextSquare(int sqr)
        {
            if (sqr < FINISH && sqr >= START)
            {
                return gameBoard[sqr + 1];
            }
            else
            {
                throw new ArgumentOutOfRangeException("Parameter is not a valid square number!");
            }
        }


    }
}
