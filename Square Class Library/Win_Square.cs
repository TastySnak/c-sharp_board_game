﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Player_Class_Library;
using Board_Class_Library;

namespace Square_Class_Library
{
    public class Win_Square : Square
    {
        public Win_Square(string name, int number)
            : base(name, number)
        {
            
        }

        public override void EffectOnPlayer(Player who)
        {
            who.Add(15);
            int new_position = who.Location.GetNumber() + who.Roll_Dice_Again();

            // Makes sure the new position never exceeds the finishing square
            if (new_position > Board.FINISH)
            {
                new_position = Board.FINISH;
            }

            who.Location = Board.GetGameBoardSquare(new_position);
            who.Location.EffectOnPlayer(who);
        }
    }
}
