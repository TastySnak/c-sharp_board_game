﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Player_Class_Library;
using Board_Class_Library;

namespace Square_Class_Library
{
    public class Lose_Square : Square
    {
        public Lose_Square(string name, int number)
            : base(name, number)
        {
            
        }

        public override void EffectOnPlayer(Player who)
        {
            who.Deduct(25);
            int new_position = who.Location.GetNumber() - 3;
            who.Location = Board.GetGameBoardSquare(new_position);
            who.Location.EffectOnPlayer(who);
        }
    }
}
