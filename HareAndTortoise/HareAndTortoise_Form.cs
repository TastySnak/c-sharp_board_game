﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Board_Class_Library;
using Player_Class_Library;
using Square_Class_Library;
using Die_Class_Library;
using System.Diagnostics;

namespace HareAndTortoise {
    public partial class HareAndTortoise_Form : Form {

        const int NUM_OF_ROWS = 8;
        const int NUM_OF_COLUMNS = 7;
        const int NUM_OF_SQUARES = 56;

        bool preRound;          // Used in UpdateSquare when clearing old tokens and displaying new tokens 

        public HareAndTortoise_Form() {
            InitializeComponent();
            HareAndTortoise_Game.SetUpGame();
            ResizeGameBoard();
            SetUpGuiGameBoard();
            dataGridView1.DataSource = HareAndTortoise_Game.Players;
            UpdateSquare();
            Trace.Listeners.Add(new ListBoxTraceListener(listBox1));
        }


        private void SetUpGuiGameBoard() 
        {
            int row, column;
        
            // for each square that is on the game board 
            for (int each = 0; each < NUM_OF_SQUARES; each++)
            {
                //      obtain the Square object associated with the square
                Square sqr = Board.GetGameBoardSquare(each);

                //      create a SquareControl object (ie call Constructor)
                SquareControl sqrCtrl = new SquareControl(sqr, HareAndTortoise_Game.Players);
                
                //      if the square is either Start square or Finish square
                if (sqr.GetNumber() == Board.START || sqr.GetNumber() == Board.FINISH)
                {
                    //         set the BackColor of the SquareControl to BurlyWood
                    sqrCtrl.BackColor = Color.BurlyWood;
                }
                    //      otherwise do not set the BackColor

                //      Determine the correct position (cell) in the TablelayoutPanel of the square
                MapSquareToTablePanel(each, out row, out column);

                //      Add the SquareControl object to that position of the TableLayoutPanel
                gameBoardPanel.Controls.Add(sqrCtrl, column, row);


            }
        }//end SetUpGuiGameBoard()

        private void ResizeGameBoard()
        {
            const int SQUARE_SIZE = SquareControl.SQUARE_SIZE;
            int currentHeight = gameBoardPanel.Size.Height;
            int currentWidth = gameBoardPanel.Size.Width;
            int desiredHeight = SQUARE_SIZE * NUM_OF_ROWS;
            int desiredWidth = SQUARE_SIZE * NUM_OF_COLUMNS;
            int increaseInHeight = desiredHeight - currentHeight;
            int increaseInWidth = desiredWidth - currentWidth;
            this.Size += new Size(increaseInWidth, increaseInHeight);
            gameBoardPanel.Size = new Size(desiredWidth, desiredHeight);
        } //end ResizeGameBoard

        public static void MapSquareToTablePanel(int number, out int row, out int column)
        {
            if (number / (NUM_OF_ROWS - 1) == 0)
            {
                row = 7;
            }
            else if (number / (NUM_OF_ROWS - 1) == 1)
            {
                row = 6;
            }
            else if (number / (NUM_OF_ROWS - 1) == 2)
            {
                row = 5;
            }
            else if (number / (NUM_OF_ROWS - 1) == 3)
            {
                row = 4;
            }
            else if (number / (NUM_OF_ROWS - 1) == 4)
            {
                row = 3;
            }
            else if (number / (NUM_OF_ROWS - 1) == 5)
            {
                row = 2;
            }
            else if (number / (NUM_OF_ROWS - 1) == 6)
            {
                row = 1;
            }
            else
            {
                row = 0;
            }

            if (number % NUM_OF_COLUMNS == 0)
            {
                if (row % 2 == 1)
                {
                    column = 0;
                }
                else
                {
                    column = 6;
                }
            }
            else if (number % NUM_OF_COLUMNS == 1)
            {
                if (row % 2 == 1)
                {
                    column = 1;
                }
                else
                {
                    column = 5;
                }
            }
            else if (number % NUM_OF_COLUMNS == 2)
            {
                if (row % 2 == 1)
                {
                    column = 2;
                }
                else
                {
                    column = 4;
                }
            }
            else if (number % NUM_OF_COLUMNS == 3)
            {
                column = 3;
            }
            else if (number % NUM_OF_COLUMNS == 4)
            {
                if (row % 2 == 1)
                {
                    column = 4;
                }
                else
                {
                    column = 2;
                }
            }
            else if (number % NUM_OF_COLUMNS == 5)
            {
                if (row % 2 == 1)
                {
                    column = 5;
                }
                else
                {
                    column = 1;
                }
            }
            else
            {
                if (row % 2 == 1)
                {
                    column = 6;
                }
                else
                {
                    column = 0;
                }
            }
        }

        /// <summary>
        /// Updates the players position on the board
        /// by either removing or placing the players tokens on the appropriate squares
        /// </summary>
        private void UpdateSquare()
        {
            for (int player = 0; player < HareAndTortoise_Game.NumberOfPlayers; player++)
            {
                Square current = HareAndTortoise_Game.Players[player].Location;
                int row, col;
                int num = current.GetNumber();

                MapSquareToTablePanel(num, out row, out col);
                SquareControl control = (SquareControl)gameBoardPanel.GetControlFromPosition(col, row);

                // if true - clears players from old position, if false - displays players in new position
                if (preRound)
                {
                    control.ContainsPlayers[player] = false;
                }
                else
                {
                    control.ContainsPlayers[player] = true;
                }
            }
            gameBoardPanel.Invalidate(true);
        }

        /// <summary>
        /// Roll Dice button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void button1_Click(object sender, EventArgs e)  // Made public to disable in PlayOneRound 
        {
            comboBox1.Enabled = false;
            button1.Enabled = false;
            button2.Enabled = false;

            // Clears the tokens from their old position
            preRound = true;
            UpdateSquare();

            bool gameStatus = HareAndTortoise_Game.PlayOneRound();

            // Displays tokens in their new position
            preRound = false;
            UpdateSquare();

            OutputPlayersDetails();
            UpdateDataGridView();

            button1.Enabled = true;
            button2.Enabled = true;

            // disables roll dice button when a player lands on the finishing square
            if (gameStatus)
            {
                button1.Enabled = false;
            }
        }

        /// <summary>
        /// Reset Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            comboBox1.Enabled = true;
            button1.Enabled = true;
            HareAndTortoise_Game.game_finished = false;

            // Clears the tokens from their old position
            preRound = true;
            UpdateSquare();

            // Reset the current players
            for (int player = 0; player < HareAndTortoise_Game.NumberOfPlayers; player++)
            {                
                HareAndTortoise_Game.Players[player].Location = Board.GetGameBoardSquare(Board.START);
                HareAndTortoise_Game.Players[player].HasWon = false;
                HareAndTortoise_Game.Players[player].Money = 100;
            }

            UpdateDataGridView();

            // Displays tokens in their new position
            preRound = false;
            UpdateSquare();

            // Reinitialise
            HareAndTortoise_Game.SetUpGame();
            HareAndTortoise_Game.max_dollars = 0;
            HareAndTortoise_Game.max_square = 0;
            HareAndTortoise_Game.winners.Clear();
            HareAndTortoise_Game.most_money.Clear();

            // This fixed the issue of the Players panel not undating after pressing the reset button
            // Only now it will show only the number of players in the game, instead of all of them!
            dataGridView1.DataSource = HareAndTortoise_Game.Players;
        }

        /// <summary>
        /// Exit Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you really want to exit?", "Are you sure?",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.None,
                MessageBoxDefaultButton.Button1);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                System.Environment.Exit(1);
            }
        }

        /// <summary>
        /// Number of players combobox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Clears the tokens currently in the start square
            preRound = true;
            UpdateSquare();

            // Disables the roll dice button until the reset button is clicked
            // and the new number of tokens appear on the start square
            button1.Enabled = false;

            HareAndTortoise_Game.NumberOfPlayers = int.Parse((string) comboBox1.SelectedItem);
            
            // Keeps game from crashing when numberOfPlayers increases from lower number of players
            HareAndTortoise_Game.InitialisePlayers();
        }

        private void OutputPlayersDetails()
        {
            HareAndTortoise_Game.OutputAllPlayerDetails();
            listBox1.Items.Add("");
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
        }

        static public void UpdateDataGridView()
        {
            HareAndTortoise_Game.Players.ResetBindings();
        }

    }//end class 
} //end namespace
